﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class StartGame : MonoBehaviour {
    //Commenting to refresh code
    public void onClick(int scene)
    {
        Debug.Log("You pressed start game!");
        SceneManager.LoadScene(scene);
    }
}
