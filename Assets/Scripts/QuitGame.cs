﻿using UnityEngine;
using System.Collections;

public class QuitGame : MonoBehaviour {

	public void OnClick()
    {
        Debug.Log("You pressed quit game!");
        Application.Quit();
    }
}
