﻿using UnityEngine;
using System.Collections;

public class DisplayInventoryCanvas : MonoBehaviour {

    public GameObject menu;
    public bool isShowing;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.I))
        {
            Debug.Log("Opening inventory!");
            isShowing = !isShowing;
            menu.SetActive(isShowing);
        }
	}
}
